import Vue from 'vue';
import Vuex from 'vuex';
import { all_questions } from "../assets/questions.js"

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    aboutShow: false, // Show or hide About menu
    categories: [], // trivia categories
    currentCategory: { // chosen category
      name: 'Random',
      id: 9,
    },
    accounts: {'admin@feelingo.com': 'Aa123456', '1': '1'},
    currentView: 'app-login',
    questions: [], // current list of questions in game
    isGameOver: false, // game state
    isPaused: false,
    round: 0, // round counter, starts at 0, ends at 9. Linked to display of current question
    scores: {
      playerOne: {
        history: [],
        total: 0,
      },
      playerTwo: {
        history: [],
        total: 0,
      }
    },
    solo: true, // Game mode, solo or multiplayer?
    starPower: false, // show starPower animation
  },
  // =============== ACTIONS ===============
  actions: {
    loginPressed(context, { username, password, nextView }) {
      if ((context.state.accounts[username] !== undefined) && (context.state.accounts[username] === password))
      {
        context.commit('changeView', nextView); 
      }
    },
    signupPressed(context, { username, password, nextView }) {
      context.state.accounts[username] = password;
      context.commit('changeView', nextView);
    },
    // Call starPower mutation and set delay toggle
    starPower(context) {
      context.commit('starPower');
      let vm = context;
      setTimeout(() => {
        vm.state.starPower = false;
      }, 1600);
    },
    // Called by Starter.vue.
    startGame(context) {
      context.state.currentView = 'app-loader';
      // Fetch batch of questions for specific category
      let api;
      // Determine if random (default) or chosen category
      if (context.state.currentCategory.name === 'Random') {
        api = 'https://opentdb.com/api.php?amount=10';
      } else {
        api = 'https://opentdb.com/api.php?amount=10&category=' + context.state.currentCategory.id;
      }
      Vue.http.get(api)
      .then(response => {
        return response.json();
      })
      .then(data => {
        context.commit('startGame', data.results);
      });
    },
    newPatient(context) {
      context.commit('changeView', 'app-intro');
    }
  },
  // ========== GETTERS ============
  getters: {
    // Get solo or multiplayer
    mode: state => {
      return state.solo;
    },
    round: state => {
      return state.round;
    },
    // Determine player turn
    turn: state => {
      let check = state.round % 2;
      if (check === 0 || state.round === 0) {
        return 'playerOne';
      } else {
        return 'playerTwo';
      }
    },
  },
  // ============ MUTATIONS ===============
  mutations: {
    changeView: (state, newView) => {
      state.currentView = newView;
    },
    // Toggle display of about menu
    aboutToggle: state => {
      state.aboutShow = !state.aboutShow;
    },
    // Set game over and show modal after 10 rounds
    isGameOver: state => {
      console.log(state.round);
      console.log(state.questions.length);

      if (state.round === state.questions.length - 1) {
        state.isGameOver = true;
      }
    },
    // Next round
    incrementRound: state => {
      state.round += 1;
    },
    // Restart game with default state
    newGame: state => {
      state.currentCategory.name = 'Random';
      state.currentCategory.id = 9;
      state.currentView = 'app-intro';
      state.solo = true;
    },
    // Pause game state and disable answer buttons after submtting answer
    pauseGame: (state, payload) => {
      if (payload === 'pause') {
        state.isPaused = true;
      } else {
        state.isPaused = false;
      }
    },
    // Reset common default game parameters
    resetGame: state => {
      state.isGameOver = false;
      state.isPaused = false;
      state.questions = [];
      state.round = 0;
      state.scores.playerOne.total = 0;
      state.scores.playerTwo.total = 0;
      state.scores.playerOne.history = [];
      state.scores.playerTwo.history = [];
    },
    // Score after answering question
    score: (state, payload) => {
      let player = payload.mode;
      if (payload.true) {
        state.scores[player].history.push({
          correct: true,
          incorrect: false,
        });
        state.scores[player].total += 1;
      } else {
        state.scores[player].history.push({
          correct: false,
          incorrect: true,
        });
      }
    },
    // Set game mode from Starter.vue
    selectMode: (state, payload) => {
      payload === true ? state.solo = true : state.solo = false;
    },
    // Set current category from Starter.vue
    setCurrentCategory: (state, payload) => {
      state.currentCategory.name = payload.name;
      state.currentCategory.id = payload.id;
    },
    // Call starPower
    starPower: (state) => {
      state.starPower = true;
    },
    startGame: (state, payload) => {
      // Set questions to payload from http request in startGame action
      state.questions = JSON.parse(JSON.stringify(all_questions));
      // console.log(all_questions)
      // console.log(state.questions)
      // console.log(state)
      // Create list of incorrect choices
      
      // Set view to game board
      state.currentView = 'app-game-board';
    },
    changeView: (state, nextView) => {
      state.currentView = nextView;
    },
    // Apply classes, which indicate correct or incorrect, to buttons after
    // submtting answer
    styleButton: (state, ans) => {
      if (ans.answer) {
        ans.classes = {correct: true};
      } else {
        ans.classes = {incorrect: true};
      }
      state.questions[state.round].choices.forEach(el => {
        if (el.answer) {
          el.classes = { correct: true }
        }
      });      
    }
  }
});
